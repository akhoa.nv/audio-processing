// Writen by Khoa Nguyen and Thanh Truong
// Audio processing with UI via terminal
// ATTENTION: this app is recommended to run on UNIX since some library or function won't work properly on Windows

#include <stdio.h>
#include <stdlib.h>
#include <sndfile.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include "kiss_fft.h"
#include <vlc/vlc.h>
#include <pthread.h>

// File I/O
#define F_OUT "sample.file"

// Color
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

char F_INP[1000];
const char *Color[7];
int f,sr,c;
int len = 0;
int u_input = 0;
int cont = 1;
char temp;

// Color to term. Idea but never implemented
void color_array(){
	Color[0] = "\x1B[0m";
	Color[1] =  "\x1B[31m";
	Color[2] =  "\x1B[32m";
	Color[3] =  "\x1B[33m";
	Color[4] =  "\x1B[34m";
	Color[5] =  "\x1B[35m";
	Color[6] =  "\x1B[36m";
}

// Clear terminal. ATTENTION: ONLY WORKS ON UNIX EVEN THOUGH IT IS WRITTEN FOR BOTH
void term_clear(){
    #if defined(__linux__) || defined(__unix__) || defined(__APPLE__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}

// Get data in
void read_audio() {
	SNDFILE *sf;
	SF_INFO info;
	int num_channels;
	int num, num_items;
	int *buf;
	int i,j;
	FILE *out;

	/* Open the WAV file. */
	info.format = 0;
	sf = sf_open(F_INP,SFM_READ,&info);

	if (sf == NULL) {
		term_clear();
		printf("%sError: Failed to open the file. Please check your file again if it exist.\n%s", KRED, KNRM);
		exit(-1);
	} else {
		term_clear();
		printf("Please wait while we process your audio file...\n");
	}

	/* Print some of the info, and figure out how much data to read. */
	f = info.frames; // how many frames that is taken
	sr = info.samplerate; // how many frames per second
	c = info.channels; // how many channels

	// #frame/sec = f/sr

	//printf("frames=%d\n",f);
	//printf("samplerate=%d\n",sr);
	//printf("channels=%d\n",c);

	num_items = f*c;

	//printf("num_items=%d\n",num_items);

	/* Allocate space for the data to be read, then read it. */
	buf = (int *) malloc(num_items*sizeof(int));
	num = sf_read_int(sf,buf,num_items);
	sf_close(sf);
	//printf("Read %d items\n",num);

	/* Write the data to filedata.out. */
	out = fopen(F_OUT,"w");

	for (i = 0; i < num; i += c){
		for (j = 0; j < c; ++j)
            fprintf(out,"%d ",buf[i+j]);
		fprintf(out,"\n");
	}

	fclose(out);
}

void scan_data(float * sampleL, float * sampleR){
	FILE *FILE_INPUTP = fopen(F_OUT, "r");
	int srL, srR;
	int i = 0;

	// Check if file exists
	if (FILE_INPUTP == NULL){
		printf("Error: There\'s no such file.\n");
		exit(0);
	} else {
		while (!feof(FILE_INPUTP)){
			fscanf(FILE_INPUTP, "%d %d\n", &srL, &srR);
			sampleL[i] = (float)srL;
			sampleR[i] = (float)srR;
			i += 1;
		}
	}
}

// Processing data
double combine_channels(float * sample, float * sampleL, float * sampleR){
	int x;

	for (x = 0; x < f; x++){
		sample[x] = sampleL[x] + sampleR[x];
	}
}

void window_funct(float * sample){
	int i;

	for (int i = 0; i < f; i++) {
		double multiplier = 0.5 * (1 - cos(2*M_PI*i/2047));
		sample[i] = multiplier * sample[i];
	}
}

void find_magn(float * magn, kiss_fft_cpx * fft_in, kiss_fft_cpx * fft_out){
	int i;

	for (int i = 0; i < 4000; i++) {
		magn[i] = sqrt(pow(fft_out[i].r,2) + pow(fft_out[i].i,2));
		magn[i] = 20*log10(magn[i]);
	}
}

float find_max(float * magn){
	int i;
	float result = 0;

	for (i = 0; i < 4000; i++){
		if (result < magn[i]){
			result = magn[i];
		}
	}

	return result;
}

// Scale and draw graph
void scale_graph(float * magn, int * graph){
	float max = find_max(magn);
	int i;

	for (i = 0; i < 4000; i++){
		graph[i] = (int)((magn[i]/max)*30);
	}
}

void draw_graph(int * graph){
	int i,k;

	for (i = 0; i < 4000; i++){
		//int r = rand() % 7;
		//printf("%s", Color[r]);

		if ((i+1) % 160 == 0){
			for(k = 0; k < graph[i]; k++){
				printf("\u25A0");
				fflush(stdout);
			}
			printf("\n");
			fflush(stdout);
		}
	}
}

void sps(float * sample_per_sec, float * sample, int j){
	int i;

	for (i = 0; i < sr/5; i++){
		sample_per_sec[i] = sample[(sr/5) + i*j];
	}
}

// Convert Float to Binary
char * floattobinary(float f){
	char *b = (char *)calloc(33, 1);
	unsigned *p = (unsigned*)(&f);
	b[33] = '\0';
	for (int i=31; i>=0; i--){
		b[i] = (*p)%2 + 48;
		(*p) /= 2;
	}

	return b;
}

// Return to menu
void menu() {
	fflush (stdout);
	scanf("%c", &temp);
	getchar();
}

// Play WAV
void *playMusic() {
	libvlc_instance_t *inst;
	libvlc_media_player_t *mp;
	libvlc_media_t *m;

	// load the engine
	inst = libvlc_new(0, NULL);
	// create a file to play
	m = libvlc_media_new_path(inst, F_INP);
	// create a media play playing environment
	mp = libvlc_media_player_new_from_media(m);
	// release the media now.
	libvlc_media_release(m);
	// play the media_player
	libvlc_media_player_play(mp);
	sleep(f/sr); // let it play for 10 seconds
	// stop playing
	libvlc_media_player_stop(mp);
	// free the memory.
	libvlc_media_player_release(mp);

	libvlc_release(inst);
}

void main() {
	srand(time(NULL));
	int i, j;

	// Init Color
	color_array();

	// User file input
	term_clear();
	printf("Enter audio file location: ");
	scanf("%s", F_INP);

	// Read data from audio
	read_audio();

	// Allocate memory for arrays with frames is define at read_audio()
	float * sampleL;
	float * sampleR;
	float * sample;

	sampleL = malloc (f * sizeof(float));
	sampleR = malloc (f * sizeof(float));
	sample = malloc (f * sizeof(float));


	// Scan data back into an array
	scan_data(sampleL, sampleR);

	// Combine both Left and Right Channel
	combine_channels(sample, sampleL, sampleR);

	// Apply Hanning Window Function
	window_funct(sample);

	// Init var
	int * graph;
	float * magn;
	float * sample_per_sec;
	kiss_fft_cpx * fft_in;
	kiss_fft_cpx * fft_out;

	kiss_fft_cfg cfg = kiss_fft_alloc(f, 0, NULL, NULL);

	while (cont == 1){
		graph = malloc (4000 * sizeof(int));
		magn = malloc (4000 * sizeof(float));
		sample_per_sec = malloc (f * sizeof(float));
		fft_in = malloc (f * sizeof(kiss_fft_cpx));
		fft_out = malloc (f * sizeof(kiss_fft_cpx));

		term_clear();

		// Intro
		printf("\033[1m      ___      _    _   _____    __    _____\n");
		printf("     / _ \\    | |  | | |  __ \\  |  |  / ___ \\\n");
		printf("    / /_\\ \\   | |  | | | |  \\ \\ |  | / /   \\ \\\n");
		printf("   / _____ \\  | |__| | | |__/ / |  | \\ \\___/ /\n");
		printf("  /_/     \\_\\ \\______/ |_____/  |__|  \\_____/\n");
		printf("		   PROCESSING\033[0m\n");
		printf("        by Khoa Nguyen and Thanh Truong\n\n");

		// Get user choice
		printf("Main menu\n");
		printf("------------------------------------------------------\n");
		printf("0: Print the 10 first data point in binary\n");
		printf("1: Render the audio power spectrum realtime\n");
		printf("2: Print the frequency graph of the whole audio file\n");
		printf("3: Information about the audio\n");
		printf("4: Change audio file\n");
		printf("5: Exit the program\n");
		printf("------------------------------------------------------\n");
		if (u_input < 0 || u_input > 5 ){
			printf("Select an appropriate number %s[0-5]%s then [Enter]: ", KRED, KNRM);
		} else {
			printf("Select an appropriate number [0-5] then [Enter]: ");
		}

		scanf("%d", &u_input);

		if (u_input == 1){
			pthread_t thread_id;
			pthread_create(&thread_id, NULL, playMusic, NULL);

			free(sample_per_sec);
			free(graph);
			free(magn);
			free(fft_in);
			free(fft_out);

			graph = malloc (4000 * sizeof(int));
			magn = malloc (4000 * sizeof(float));
			sample_per_sec = malloc ((sr/5) * sizeof(float));
			fft_in = malloc ((sr/5) * sizeof(kiss_fft_cpx));
			fft_out = malloc ((sr/5) * sizeof(kiss_fft_cpx));

			j = 0;
			kiss_fft_cfg cfg = kiss_fft_alloc(sr/5, 0, NULL, NULL);

			while (j < ((int)(f/sr) * 5)){
				term_clear();
				sps(sample_per_sec, sample, j);

				// Assign sample array to FFT in array to perform FFT
				for (i = 0; i < sr/5; i++) {
			        fft_in[i].r = sample_per_sec[i];
			        fft_in[i].i = 0;
			        fft_out[i].r = 0;
			        fft_out[i].i = 0;
			 	}

				// Apply FFT
				kiss_fft(cfg,fft_in,fft_out);

				// Find magnitude of the data out to get Level (dB) / Freq (Hz)
				find_magn(magn, fft_in, fft_out);

				scale_graph(magn, graph);
				draw_graph(graph);

				usleep(190000);

				j++;
			}
			pthread_join(thread_id, NULL);

			free(sample_per_sec);
			free(graph);
			free(magn);
			free(fft_in);
			free(fft_out);
		} else if (u_input == 0){
			term_clear();
			for (i = 0; i < 10; i++) {
				printf("%s\n\n", floattobinary(sample[i]));
			}
			printf("\nPress [Enter] to continue...\n");
			menu();
		} else if (u_input == 2){
			term_clear();

			free(graph);
			free(magn);
			free(fft_in);
			free(fft_out);

			fft_in = malloc (f * sizeof(kiss_fft_cpx));
			fft_out = malloc (f * sizeof(kiss_fft_cpx));
			graph = malloc (4000 * sizeof(int));
			magn = malloc (4000 * sizeof(float));

			kiss_fft_cfg cfg = kiss_fft_alloc(4000, 0, NULL, NULL);

			for (i = 0; i < 4000; i++) {
			   fft_in[i].r = sample[i];
			   fft_in[i].i = 0;
			   fft_out[i].r = 0;
			   fft_out[i].i = 0;
			}

			// Apply FFT
			kiss_fft(cfg,fft_in,fft_out);

			// Find magnitude of the data out to get Level (dB) / Freq (Hz)
			find_magn(magn, fft_in, fft_out);

			scale_graph(magn, graph);
			draw_graph(graph);

			printf("\nPress [Enter] to continue...\n");

			menu();
		} else if (u_input == 3){
			term_clear();
			printf("Frames = %d\n",f);
			printf("Sample Rate = %d\n",sr);
			printf("Channels = %d\n",c);
			printf("\nPress [Enter] to continue...\n");

			menu();
		} else if (u_input == 4){
			term_clear();
			printf("Enter audio file location: ");
			scanf("%s", F_INP);

			read_audio();
			scan_data(sampleL, sampleR);
			combine_channels(sample, sampleL, sampleR);
			window_funct(sample);
		} else if (u_input == 5){
			cont = 0;
		} else {
			term_clear();
		}
	}
	// len output of the magnitude
	/*for (int i = 0; i < len; i++) {
		printf("%d dB\n", graph[i]);
	}*/

	//printf("%d", len);

	// Free allocated memory
	free(cfg);
	free(sampleL);
	free(sampleR);
	free(sample);

	free(sample_per_sec);
	free(graph);
	free(magn);
	free(fft_in);
	free(fft_out);
	exit(0);
}
